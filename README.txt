What is ASCII Art?
==================
ASCII Art is a module to allow for easy insertion of ASCII Art code signing.
The art it automatically added to all pages via a custom post-render hook 
that prepends the art prior to the HEAD tag. To view an example of ASCII Art
in action, check out the following sites:
	* http://www.barackobama.com/

ASCII Art Settings
==================
The settings page is located under the store menu at 
/admin/config/development/ascii-art

Roadmap
========
Add in image to ASCII converter

Sponsors
=========
This project is currently sponsored by Ciplex. Ciplex is a full-service interactive and marketing agency that creates award winning design and development solutions, and is located in Los Angeles, California. http://ciplex.com